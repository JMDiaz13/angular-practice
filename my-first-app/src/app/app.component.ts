import { Component } from '@angular/core';
import { WebSocketAPI } from 'src/services/WebSocketAPI';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  // styles: [`
  // h3 {
  //   color: red;
  // }`]
})
export class AppComponent {
  title = 'angular-springboot-websocket';

  webSocketAPI: WebSocketAPI;
  greeting: any;
  name: string;

  constructor() { }

  ngOnInit() {
    this.webSocketAPI = new WebSocketAPI(new AppComponent())
  }

  connect() {
    this.webSocketAPI._connect();
  }

  disconnect() {
    this.webSocketAPI._disconnect();
  }

  sendMessage() {
    this.webSocketAPI._send(this.name);
  }

  handleMessage(message) {
    this.greeting = message;
  }
}
